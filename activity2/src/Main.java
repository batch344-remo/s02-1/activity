import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] primeArray = new int[5];

        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        System.out.println("The first prime number is: " + primeArray[0] + ".");
        System.out.println("The second prime number is: " + primeArray[1] + ".");
        System.out.println("The third prime number is: " + primeArray[2] + ".");
        System.out.println("The fourth prime number is: " + primeArray[3] + ".");
        System.out.println("The fifth prime number is: " + primeArray[4] + ".");

        String[] friends = {"John", "Jane", "Chloe", "Zoey"};
        System.out.println("My friends are: " + Arrays.toString(friends));

        HashMap<String, Integer> generics = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our inventory consists of: " + generics);
    }
}